/*Default textmode driver for SISA16.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SISA16_DISK_SIZE_GIGS 2
static const size_t DISK_ACCESS_MASK = (SISA16_DISK_SIZE_GIGS*1024*1024*4)-1;

static unsigned short shouldquit = 0;


#include "isa_pre.h"




/*
	textmode driver.
*/

FILE* s16dsk = 0;

static void init_disk(){
    s16dsk = fopen("sisa16.dsk", "wb");
    if(!s16dsk) goto err;
    size_t i = 0;
    for(;i<((DISK_ACCESS_MASK+1)*256);i++)if(EOF == fputc(0, s16dsk)) goto err;
        //while((unsigned long)ftell(s16dsk) < (unsigned long)DISK_ACCESS_MASK) 
    fflush(s16dsk);
    fclose(s16dsk);
    return;
    err:;
    puts("SISA16: FAILED TO INIT DISK");
    exit(1);
    
}
static void di(){
    sisa16_alloc_segment();
	s16dsk = fopen("sisa16.dsk", "rb+");
	if(!s16dsk) init_disk(); else return;
	s16dsk = fopen("sisa16.dsk", "rb+");
	if(!s16dsk){
	    puts("SISA16: FAILED TO OPEN DISK");
	    exit(1);
	}
    return;
}
static void dcl(){free(SEGMENT);return;}



/*
	IMPLEMENT YOUR CLOCK HERE!!!!
	a must be milliseconds
	b must be seconds
	c is presumed to be some raw measurement of the clock- it can be whatever
*/
#define clock_ins(){\
	size_t q;\
	{\
		STASH_REGS;\
		q=clock();\
		UNSTASH_REGS;\
	}\
	a=((q)/(CLOCKS_PER_SEC/1000));\
	b=q/(CLOCKS_PER_SEC);\
}


static unsigned short gch(){
	return 0xff & getchar();
}


static void pch(unsigned short a){
	putchar(a);
}

static unsigned short DONT_WANT_TO_INLINE_THIS interrupt(unsigned short a,
									unsigned short b,
									unsigned short c,
									unsigned short stack_pointer,
									unsigned short program_counter,
									unsigned char program_counter_region,
									UU RX0,
									UU RX1,
									UU RX2,
									UU RX3,
									u* M
								)
{
	if(a == 0x80) return 0x80; /*Ignore 80- it is reserved for system calls!*/
	if(a == 1){
		return 0;
	}
	if(a=='\n') {
		fflush(stdout);
		return a;
	}
	if(a == 0xc){
		printf("\e[H\e[2J\e[3J");
		return a;
	}
	if(a==0xE000){
		return 0;
	}
	if(a==0xE001){
		return 1;
	}
	if(a == 0xffFF){ /*Perform a memory dump.*/
		unsigned long i,j;
		for(i=0;i<(1<<24)-31;i+=32)
			for(j=i,printf("%s\r\n%06lx|",(i&255)?"":"\r\n~",i);j<i+32;j++)
					printf("%02x%c",M[j],((j+1)%8)?' ':'|');
		return a;
	}
	if(a == 0xFF10){ /*Read 256 bytes from saved disk to page b*/
		size_t location_on_disk = ((size_t)RX0) << 8;
		location_on_disk &= DISK_ACCESS_MASK;
		fseek(s16dsk, location_on_disk, SEEK_SET);
		{
			UU i = 0;
			for(i = 0; i < 256; i++){
				M[(((UU)b)<<8) + i] = fgetc(s16dsk);
			}
		}
		return 1;
	}

	if(a == 0xFF11){ /*write 256 bytes from page 'b' to saved disk.*/
		size_t location_on_disk = ((size_t)RX0) << 8;
		location_on_disk &= DISK_ACCESS_MASK;
		fseek(s16dsk, location_on_disk, SEEK_SET);
		{
			UU i = 0;
			for(i = 0; i < 256; i++){
				fputc(M[((UU)b<<8) + i], s16dsk);
			}
		}
		return 1;
	}
	if(a == 0xFF12){
	    M[RX0]   = DISK_ACCESS_MASK >> 24;
	    M[RX0+1] = DISK_ACCESS_MASK >> 16;
	    M[RX0+2] = DISK_ACCESS_MASK >> 8;
	    M[RX0+3] = DISK_ACCESS_MASK;
	    return 1;
	}
	if(a == 0xFF13){
	    char* d = RX0 + (char*)M;
	    strcpy(d, "D_SIMPLE C STD DRIVER::DISK");
	}
	return a;
}
