/*

POSIX system driver for SISA16

    TODO: Implement OpenGL GPU for supported systems

*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lockstepthread.h"
#include "MHS.h"

extern void disk_open();
extern void disk_close();
extern void store_sector_bootloader(MHS_UINT where, sector* s);
extern MHS_UINT get_disk_size();


static unsigned short shouldquit = 0;


#ifndef NO_SIGNAL
#include <signal.h>
#define TRAP_CTRLC signal(SIGINT, emu_respond);
void emu_respond(int bruh){
	(void)bruh;
	shouldquit = 0xffFF;
	return;
}

#else

#define TRAP_CTRLC /*a comment.*/

#endif



#include "isa_pre.h"
/*
	buffers for stdout and stdin.
*/
static char stdout_buf[
    0x10000 * 16
] = {0};




/*
	textmode driver.
*/

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
static struct termios oldChars;
static struct termios newChars;


static void initTermios(int echo) 
{
  tcgetattr(STDIN_FILENO, &oldChars); /* grab old terminal i/o settings */
  newChars = oldChars; /* make new settings same as old settings */
  newChars.c_lflag &= ~ICANON; /* disable buffered i/o */
  newChars.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &newChars); /* use these new terminal i/o settings now */
}


static void dieTermios(){
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &oldChars); /* use these new terminal i/o settings now */	
}


/*
#include "AVI.h"
*/



static void DONT_WANT_TO_INLINE_THIS di(){
	sisa16_alloc_segment();
	initTermios(0);
	atexit(dieTermios);
	TRAP_CTRLC
	setvbuf ( stdout, stdout_buf, _IOFBF, sizeof(stdout_buf));
	disk_open();
}
static void dcl(){
    free(SEGMENT);
    disk_close();    
}


/*
	IMPLEMENT YOUR CLOCK HERE!!!!
	a must be milliseconds
	b must be seconds
*/
#define clock_ins(){\
	size_t q;\
	{\
		STASH_REGS;\
		q=clock();\
		UNSTASH_REGS;\
	}\
	a=((q)/(CLOCKS_PER_SEC/1000));\
	b=q/(CLOCKS_PER_SEC);\
}


static unsigned short gch(){
	return 0xff & getchar_unlocked();
}
static void pch(unsigned short a){
	putchar_unlocked(a);
}



static unsigned short DONT_WANT_TO_INLINE_THIS interrupt(unsigned short a,
									unsigned short b,
									unsigned short c,
									unsigned short stack_pointer,
									unsigned short program_counter,
									unsigned char program_counter_region,
									UU RX0,
									UU RX1,
									UU RX2,
									UU RX3,
									u* M
								)
{
	if(a == 0x80) return 0x80; /*Ignore 80- it is reserved for system calls!*/
	if(a == 0x81){
		printf("\r\nDEBUG INTERRUPT CALLED WITH A = %u, B = %u, PROGRAM COUNTER = %u\r\n", (unsigned)a, (unsigned)b, (unsigned)program_counter);
		return a;
	}
	if(a == 1){
		return shouldquit;
	}
	if(a=='\n') {
		fflush(stdout);
		return a;
	}
	if(a == 0xc){
		printf("\e[H\e[2J\e[3J");
		return a;
	}
	if(a==0xE000){
		fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);return 1;
	}
	if(a==0xE001){
		fcntl(STDIN_FILENO, F_SETFL, 0);return 1;
	}
	if(a == 0xffFF){ /*Perform a memory dump.*/
		unsigned long i,j;
		for(i=0;i<(1<<24)-31;i+=32)
			for(j=i,printf("%s\r\n%06lx|",(i&255)?"":"\r\n~",i);j<i+32;j++)
					printf("%02x%c",M[j],((j+1)%8)?' ':'|');
		return a;
	}
	/*
	if(a == 0xFF10){
		size_t location_on_disk = ((size_t)RX0) << 8;
		location_on_disk &= DISK_ACCESS_MASK;
		fseek(s16dsk, location_on_disk, SEEK_SET);
		{
			UU i = 0;
			for(i = 0; i < 256; i++){
				M[(((UU)b)<<8) + i] = fgetc(s16dsk);
			}
		}
		return 1;
	}
	if(a == 0xFF11){
		size_t location_on_disk = ((size_t)RX0) << 8;
		location_on_disk &= DISK_ACCESS_MASK;
		fseek(s16dsk, location_on_disk, SEEK_SET);
		{
			UU i = 0;
			for(i = 0; i < 256; i++){
				fputc(M[((UU)b<<8) + i], s16dsk);
			}
		}
		return 1;
	}
	*/
	//read sector
	if(a == 0xFF10){
        sector qq = load_sector(RX0);
        memcpy(M + (((UU)b)<<8), qq.data, 256);
        return 1;
    }
    //write sector
	if(a == 0xFF11){
        sector qq;
        memcpy(qq.data, M + (((UU)b)<<8), 256);
        store_sector(RX0, &qq);
        return 1;
	}
	if(a == 0xFF12){
        M[RX0]   = get_disk_size() >> 24;
        M[RX0+1] = get_disk_size() >> 16;
        M[RX0+2] = get_disk_size() >> 8;
        M[RX0+3] = get_disk_size();
        return 1;
	}
	if(a == 0xFF13){
        char* d = RX0 + (char*)M;
        strcpy(d, "D_TERMIOS POSIX DRIVER::NONBLOCKING_GETCHAR::DISK");
	}
	//write bootloader sector
    if(a == 0xFF14){
        sector qq;
        memcpy(qq.data, M + (((UU)b)<<8), 256);
        store_sector_bootloader(RX0, &qq);
        return 1;
    }
    //file_read_node
    if(a == 0xFF15){
        //RX0: absolute path of node
        //RX1: address of four byte variable to store secid
        //b: page address of where to write the fsnode
        sector qq;
        UU rval; //secid
        char rr =
        file_read_node(
        	(char*)M + RX0, //path
        	&rval,
        	&qq             //sectorptr
        );
        if(rr){
            memcpy(M + (((UU)b)<<8), qq.data, 256);
            M[RX1]   = rval >> 24;   
            M[RX1+1] = rval >> 16;
            M[RX1+2] = rval >> 8;
            M[RX1+3] = rval;
        }
        return rr;
    }
    /*
    file_createempty(
    	const char* path, 
    	const char* fname,
    	MHS_UINT owner,
    	MHS_USHRT permbits 
    )
    */
    if(a == 0xFF16){
        //RX0: path
        //RX1: fname
        //RX2: owner id
        //b: permbits
        UU _RX2 = RX2;
        char rr = file_createempty((char*)M+RX0, (char*)M+RX1, _RX2, b);
        return rr;
    }
    /*
    file_get_dir_listing(
    	const char* path,
    	char* buf,
    	UU max_chars  //(including null terminator)
    )
    */
    if(a == 0xFF17){
        char rr = file_get_dir_listing((char*)M+RX0, (char*)M+RX1, RX2);
        return rr;
    }
    //file_realloc
    if(a == 0xFF18){
        char rr = file_realloc((char*)M+RX0, RX1);
        return rr;
    }
    //file_remove
    if(a == 0xFF19){
        char rr = file_delete(
        	(char*)M+RX0,
        	(char*)M+RX1
        );
        return rr;
    }
    //check if fsnode exists in a directory, useful for checking if a file rename can occur
    if(a == 0xFF1A){
        char rr = node_exists_in_directory(RX0, (char*)M+RX1);
        return rr;
    }
    if(a == 0xFF1B){
        char rr = file_get_dir_entry_by_index(
        	(char*)M+RX0,
        	RX1,
        	(char*)M+RX2
        );
        return rr;
    }
	return a;
}
