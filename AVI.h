
/*
    PLAN: write the audio, video, and input handling device here in its own thread.

*/



#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <AL/alc.h>
#include <AL/al.h>
#include <SDL2/SDL_ttf.h>


static SDL_Window *sdl_win = NULL;
static SDL_GLContext* sdl_glcontext = NULL;
ALCdevice* alcdev = 0;
ALCcontext* alcctx = 0;
static int is_window_open = 0;
ALfloat listener_orientation[6] = {0,0,1,0,1,0};

/*

unsigned char ttf_buffer[1<<20];
#define TEMP_BITMAP_DIM 1024
unsigned char temp_bitmap[TEMP_BITMAP_DIM*TEMP_BITMAP_DIM];

typedef struct{
    stbtt_bakedchar cdata[96]; // ASCII 32..126 is 95 glyphs
    unsigned ftex;
} rfont;


static unsigned char* loadFont(unsigned char* fname, float pxsz)
{
    FILE* f = fopen((char*)fname, "rb");
    if(f == NULL){
        puts("<FONT LOADING ERROR>");
        puts("Could not open font:");
        puts((char*)fname);
        exit(1);
    }
    rfont* ft = malloc(sizeof(rfont));
    fread(ttf_buffer, 1, 1<<20, f);
    stbtt_BakeFontBitmap(ttf_buffer,0, pxsz, temp_bitmap,
    TEMP_BITMAP_DIM,
    TEMP_BITMAP_DIM, 
    32,96, ft->cdata); // no guarantee this fits!
    // can free ttf_buffer at this point
    glGenTextures(1, &ft->ftex);
    glBindTexture(GL_TEXTURE_2D, ft->ftex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, 
    TEMP_BITMAP_DIM,
    TEMP_BITMAP_DIM, 
    0, GL_ALPHA, GL_UNSIGNED_BYTE, temp_bitmap);
    // can free temp_bitmap at this point
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    return (unsigned char*)ft;
}

static void renderTextMono(unsigned char *text, unsigned char* fontin, float x, float y, float spacing, float vert)
{
    rfont* ft = (rfont*)fontin;
    // assume orthographic projection with units = screen pixels, origin at top left
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, ft->ftex);
    glBegin(GL_QUADS);
    float w;
    float yoff = 0;
    float x_orig = x;
    float y_orig = y;

    while (*text) {
        w = x;
        y = y_orig + yoff;
        if (*text >= 32 && *text < 128) {
            stbtt_aligned_quad q;
            stbtt_GetBakedQuad(ft->cdata, TEMP_BITMAP_DIM,TEMP_BITMAP_DIM, *text-32, &x,&y,&q,1);//1=opengl & d3d10+,0=d3d9
            q.x1 = q.x1 - q.x0;
            q.x0 = w;
            q.x1 = w + q.x1;
            glTexCoord2f(q.s0,q.t0); glVertex2f(q.x0,q.y0);
            glTexCoord2f(q.s1,q.t0); glVertex2f(q.x1,q.y0);
            glTexCoord2f(q.s1,q.t1); glVertex2f(q.x1,q.y1);
            glTexCoord2f(q.s0,q.t1); glVertex2f(q.x0,q.y1);
        }
        ++text;
        x = w + spacing;
        if(text[0] == '\n') {yoff= yoff + vert;w = x_orig;x = x_orig;}
    }
    glEnd();
}
*/

void open_AVDEV(unsigned w, unsigned h){
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_JOYSTICK) < 0)
    {
        printf("SDL2 could not be initialized!\n"
               "SDL_Error: %s\n", SDL_GetError()
        );
        exit(1);
    }
    sdl_win = SDL_CreateWindow(
        "SISA16 A/V/I",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		w, 
		h,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
	);
	if(!sdl_win)
	{
		printf("SDL2 window creation failed.\n"
			"SDL_Error: %s\n", SDL_GetError());
		exit(1);
	}
	sdl_glcontext = SDL_GL_CreateContext(sdl_win);
    if(!sdl_glcontext){
		printf("SDL2 GL Context creation failed.\n"
		"SDL_Error: %s\n", SDL_GetError());
		exit(1);
	}
	SDL_GL_MakeCurrent(sdl_win, sdl_glcontext);
	alcdev = alcOpenDevice(0);
	if(!alcdev){
	    puts("<alcOpenDevice(0) failed>");
	    exit(1);
	}
	alcctx = alcCreateContext(alcdev, 0);
	if(!alcctx){
        puts("<alcCreateContext(alcdev,0) failed>");
        exit(1);
    }
    if(!alcMakeContextCurrent(alcctx)){
        puts("<alcMakeContextCurrent failed>");
        exit(1);
    }
    alListener3f(AL_POSITION, 0,0,1);
    alListener3f(AL_VELOCITY, 0,0,0);
    alListenerfv(AL_ORIENTATION, listener_orientation);
    //TODO work with openal, get raw PCM data playing
    //>> https://ffainelli.github.io/openal-example/
    
    //TODO delete this after ive implemented event pumping
	/*
	__CBAS__appInit();
	while(!shouldquit){
		pollevents();
		__CBAS__appUpdate();
	}
	__CBAS__appClose();
	Mix_CloseAudio();
    SDL_Quit();
    */
}

void close_AVDEV(){
    if(sdl_glcontext)
        SDL_GL_DeleteContext(sdl_glcontext);
    if(sdl_win)
        SDL_DestroyWindow(sdl_win);
    sdl_win = NULL;
    sdl_glcontext = NULL;
    SDL_Quit();
}


