

//the first available page in memory.
//note that 0x100 is the start of the exec region, and 0x200 is the first page of the libc, 0x2FF is the last page of the libc.
//0x2000 gives 31 regions for code (region 0 is the stack)
//all the user has to do is modify this before calling alloc for the first time if they need more space.

:libc_alloc_start_page:
bytes %/0x2000%;

//the last available page in memory.
//It can be allocated!
:libc_alloc_last_page:
bytes %/0xFFFF%;


..export"libc_alloc_start_page"
..export"libc_alloc_last_page"

.libc_ld_start_page: farldrx0 %~LIBC_REGION%, %libc_alloc_start_page%;
.libc_st_start_page: farstrx0 %~LIBC_REGION%, %libc_alloc_start_page%;
.libc_ld_last_page: farldrx0 %~LIBC_REGION%, %libc_alloc_last_page%;
.libc_st_last_page: farstrx0 %~LIBC_REGION%, %libc_alloc_last_page%;
..export"libc_ld_start_page"
..export"libc_st_start_page"
..export"libc_ld_last_page"
..export"libc_st_last_page"


..decl_lproc:proc_alloc_reset_allocation_bitmap
	//first, calculate how much we want to reset.
	ldrx0 %libc_alloc_last_page%;
	ldrx1 %libc_alloc_start_page%;
	rxsub;
	//rx0 now contains the number of pages
	
	//get a number of bytes out of that (the number) each page has two bits, so we divide the number of pages by 4.
	//we have to add 3 first, to make sure we don't miss a byte...
	lb 3; rx1b; rxadd;
	//now divide by four to get the number of bytes
	lb 2; rx1b; rxrsh;
	rx2_0; //store for later
	//add the location of the alloc page map
	llb %libc_alloc_page_map%; rx1b;
	rxadd
	//extra byte because we are going to use a less than compare not lte
	rxincr
	//store that in RX1 for our uses.
	rx1_0;
	//begin iterating! 
	[for]{lrx0 %/libc_alloc_page_map%;}\//our pointer to the byte array is in rx0
	{rxcmp;nota;}\//as long as rx0 is less than rx1...
	{crx0;lb 0;istb;rxincr;}//write 0 to that byte.
	
	ret


//From the (relative) page number in RX0, load page bits into the "A" register.
//the exact byte is RX0/4 (2 bits per page), which must be added to the alloc_page_map address.
//the 2 bits to select are determined by the last 2 bits of the page number.
//this macro clobbers RX0, RX1, and A,B, and C.

.libc_ld_page_bits: 	rx0push;										\//save relative page number
	lb 2; rx1b; rxrsh; 													\//divide it by four
	lrx1 0,%~LIBC_REGION%, %libc_alloc_page_map%; rxadd;	cbrx0;		\//add page map address
	farilda; ca;														\//load byte into a and save in c
	rx0pop;																\//use the low bits of RX0 to get bits in a
	arx0;lb 3;and;lb 2;mul;ba;ac;rsh;lb 3;and; 							\//extract bits
	

//This one is more complicated. A holds the bits loaded from ld_page_bits, or set by hand.
//Now, we want to *write* to the page table, indicated by RX0.
//The address of the computed byte is stored this time in RX1 (that's the rx1_0 insn.)
//We stash away the loaded byte after farilda-ing it into register c.
//we restore RX0.
//We then compute the correct shift the same way we did with ld_page_bits,
//We modify our requested set of bits
//We then modify the bit pattern of the byte using a mask, we AND it to zero.
//Finally, we 'or' in the a value.
//Then we stash away our completed byte!

.libc_st_page_bits:		apush; rx0push;										\//Store bits and page number
	lb 2; rx1b; rxrsh; 														\//divide page number in RX0 by 4 (byte)
	lrx1 0,%~LIBC_REGION%, %libc_alloc_page_map%; rxadd;rx1_0;				\//get address of byte in bitmap, store in RX1
	cbrx0;farilda; ca;														\//load byte from page map, store in c.
	rx0pop;																	\//get back page number
	arx0;lb 3;and;lb 2;mul;ba;												\//convert low bits of address (XX) into offset inside the byte in the page map
	apop;lsh;apush;															\//modify input bits to be in correct position to be written to the page table byte
	la 3;lsh;compl;															\//generate inverted mask for bits of page table, to remove 
	bc;and;																	\//mask the byte from the page table from c
	ba;apop;or;																\//write the bits to the byte
	rx0_1;cbrx0;farista;													\//store the result.
	

//ALLOC~~~~~~~~~~~~~~~~~~~~~~~
//Clobbers: [all]
//Only argument: in RX0, the number of contiguous bytes needed.
//the return value is also in RX0.
//the return value is 0 if a suitable area of memory could not be found.
//the return value is otherwise a pointer to the first page of the allocated memory.
..decl_farproc(LIBC_REGION):proc_alloc
..export"proc_alloc"
	//How many pages do we need? adding 255 is the classic round-up
	lb 0xff; rx1b; rxadd;
	lb 8; rx1b; rxrsh;
	//Push it onto the stack.
	rx0push;
	//Linearly search the page map. First, initialize loop variables
	lrx2 %/0%; //The page we are investigating.
	lrx3 %/0%; //The number of free pages.
	libc_alloc_search_looptop:
		//First we need to know if we have reached past the end of the page map.
		
		rx0_2;
		ldrx1 %libc_alloc_last_page%;
		rxcmp;
		gt;
		jeq %libc_alloc_loopend_failure%;		//If we have gone past the last page, die.
		libc_ld_page_bits;//Grab those page bits!
		
		rx0_2;rxincr;rx2_0;
		nota;jeq %libc_alloc_found_free_page%; //jmpifeq;

		//FAILURE!
		lb 0;rx3b;
		jim %libc_alloc_search_looptop%;
	libc_alloc_found_free_page:
		rx0_3;rxincr;rx3_0;
		//we should now check if that is enough pages.
		rx1pop;
		rxcmp; 
		jeq %libc_alloc_loopend_success%;
		rx1push;
		jim %libc_alloc_search_looptop%;
	libc_alloc_loopend_success:
		//number of pages needed was
		//stored in RX1, which was previously stored on the stack.
		//it is overwritten here.
		rx0_2; //last page we checked
		rx1_3; //number of contiguous pages
		rxsub; //calculate first page to own
		rx2_0; //save first page to own
		rx0push; //save it on the stack too
		
		//we must must mark all the pages as used, starting with the first one.
		//it gets 0b11 (3) stored in its page bits.
		la 3;
		libc_st_page_bits;
		rx0_2;rxincr;rx2_0;
		rx0_3;rxdecr;rx3_0;
		lb 0;rx1b;rxcmp;jeq %libc_alloc_final%;		//only needed a single page!
		libc_alloc_mark_used_looptop:
			rx0_2;la 1;
			libc_st_page_bits;
			rx0_2;rxincr;rx2_0;
			rx0_3;rxdecr;rx3_0;
			lb 0;rx1b;rxcmp;
			jeq %libc_alloc_final%;
			jim %libc_alloc_mark_used_looptop%;
		libc_alloc_final:
			//convert first page address to byte address
			//note that it isn't actually an address yet, it's an offset
			//from libc_alloc_start_page
				rx0pop; //this is the first page to own that we stored on the stack
				//<Note that now, there is nothing on the stack>
				lb 8; rx1b; rxlsh; //multiply by 256 to get a byte address
				rx3_0; //save in RX3
			//we need to compute the start page as a byte address too and
			//add it to the first page index
				ldrx0 %libc_alloc_start_page%; 
				rxlsh;
				rx1_3;
				rxadd;
			//return!
				farret
	libc_alloc_loopend_failure:
		rx0pop
		lb 0;rx0b;
		farret

//FREE~~~~~~~~~~~~~~~~~~~~~
//Clobbers: [all]
//Mark an allocation as free'd
//RX0: start address to begin free-ing.
//return value: 1 in a if the operation succeeded,
//0 in a if it failed.
..decl_farproc(LIBC_REGION):proc_free
..export"proc_free"
	//modify the argument to page indexing.
		lb 8;rx1b;rxrsh;
		rx2_0;
	//bad page numbers. Greater than the end, or less than the beginning? Can't!
		ldrx1 %libc_alloc_last_page%;
		rxcmp;lb 2;cmp;jeq %libc_free_fail%;

		ldrx1 %libc_alloc_start_page%;
		rxcmp;nota;jeq %libc_free_fail%;

	//set up our loop variable.
		rx0_2;
		ldrx1 %libc_alloc_start_page%;
		rxsub;
		rx2_0;
	//First, we check to make sure that the bits are indeed set to 3.
		libc_ld_page_bits;
		lb 3; cmp; jne %libc_free_fail%;
		rx0_2;
		la 0;
		libc_st_page_bits;
		rx0_2;rxincr;rx2_0;
	//Now, the loop!
		libc_setter_looptop:
			rx0_2;
			libc_ld_page_bits;
			lb 1; cmp; jne %libc_free_success%;
			rx0_2;
			la 0;
			libc_st_page_bits;
			rx0_2;rxincr;rx2_0;
			jim %libc_setter_looptop%;
	libc_free_fail:
		la 0;
		farret

	libc_free_success:
		la 1;
		farret



//For every page, we keep 2 bits to indicate...
//1) is it allocated?
//2) is it the start of an allocation?
//they are bit 1 and 2 respectively.

//See libc.hasm
