#!/usr/bin/sisa16_asm -run

//This small program demonstrates a few essential features of Sisa16 and KRENEL
//Notably...
//1) usage of putchar and libc proc_wait
//2) the exec syscall
//3) Multitasking.

.ZERO_STACK_POINTER:		astp;popa;
//..(2):
..include"libc.hasm"

//..include"libc_pre.hasm"
//..(LIBC_REGION):

//..dinclude"libc_pre.bin"

.wait_time:				60

..main(3):
lrx0 %/krenel_boot%;
proc_krenel;
halt;

..(4):
krenel_boot:
    lrx1 %/0%;
    lrx0 %/0%;
    sc %lbl%
:lbl:
    rxincr
    rxcmp
    jmpifneq



